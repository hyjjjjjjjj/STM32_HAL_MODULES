/*
 * key.h
 *
 *  Created on: Jul 3, 2020
 *      Author: HYJ
 */

#ifndef INC_KEY_H_
#define INC_KEY_H_

#include "main.h"
#include "gpio.h"

enum{
	L_PRES_ENABLE=0,L_PRES_DISABLE
}Press_Mode;

typedef struct KEY_COMMON {
	GPIO_COMMON *pin;
	u8 mode;
	uint32_t pull;
	u8 pin_count;
	u32 press_time;
	void (*append)(struct KEY_COMMON *this, GPIO_TypeDef *port, uint32_t pin);
	u8 (*Scan)(struct KEY_COMMON *this);
} KEY_COMMON;

//KEY类初始化
KEY_COMMON* new_Key(struct KEY_COMMON *this, GPIO_TypeDef *port, uint32_t pin,
		uint32_t pull, uint8_t mode);

#endif /* INC_KEY_H_ */
