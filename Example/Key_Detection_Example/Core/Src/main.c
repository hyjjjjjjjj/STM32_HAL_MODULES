/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 * 					实现PB5 驱动LED 进行频率为2Hz的闪烁
 ******************************************************************************
 *  Created on: Jun 29, 2020
 *      Author: HYJ
 *******************************************************************************/

#include "main.h"
#include "gpio.h"
#include "key.h"
#include "usart.h"

void SystemClock_Config(void);

int main(void) {
	u32 count = 0;
	HAL_Init();
	SystemClock_Config();
	KEY_COMMON *key1 = new_Key(key1, GPIOA, GPIO_PIN_0, GPIO_PULLDOWN,
			L_PRES_ENABLE);																									//初始化第一个按键对象 将PA0添加至该对象
	KEY_COMMON *key2 = new_Key(key2, GPIOA, GPIO_PIN_1, GPIO_PULLUP,
			L_PRES_DISABLE);																									//初始化第二个按键对象 将PA1添加至该对象
	key2->append(key2, GPIOA, GPIO_PIN_2);																	//将PA2作为第二个按键加入第二个按键对象中

	UART_COMMON *uart1_common = new_Uart(uart1_common, USART1, 115200);			//初始化串口一 注册相关成员函数
	uart1_common->UPrintf(uart1_common, "KEY PRESS EXAMPLE!\r\n");							//串口输出
	while (1) {
		if (key1->mode == L_PRES_DISABLE) {																		//判断当前是否处于连按模式
			if (key1->Scan(key1) == 1) {																					//扫描第一个按键对象中的所有按键  (PA0)
				count++;																											//连按模式，非阻塞
				uart1_common->UPrintf(uart1_common, "KEY1 Press!\t count: %d\r\n",count);
			}
		} else {																														//非连按模式（也就是带松开按键的检测），阻塞，并记录按下的时间至对象的press_time成员中
			if (key1->Scan(key1) == 1) {																					//扫描第一个按键对象中的所有按键  (PA0)
				uart1_common->UPrintf(uart1_common, "KEY1 Press!\t Long Press Time:%d ms \r\n",key1->press_time * 10);
				key1->press_time = 0;
			}
		}
		if (key2->Scan(key2) == 1) {																						//扫描第二个按键对象中的所有按键  (PA1|PA2)，若检测到第一个按键被按下
			uart1_common->UPrintf(uart1_common, "Change to L_PRES_ENABLE\r\n");			//将第一个按键对象设置为非连按模式
			key1->mode = L_PRES_ENABLE;
		} else if (key2->Scan(key2) == 2) {																			//扫描第二个按键对象中的所有按键  (PA1|PA2)，若检测到第二个按键被按下
			uart1_common->UPrintf(uart1_common, "Change to L_PRES_DISABLE\r\n"); 		//将第一个按键对象设置为连按模式
			key1->mode = L_PRES_DISABLE;
			count=0;
		}
		HAL_Delay(200);
	}
}

/**
 * @brief System Clock Configuration
 * @retval None
 */
void SystemClock_Config(void) {
	RCC_OscInitTypeDef RCC_OscInitStruct = { 0 };
	RCC_ClkInitTypeDef RCC_ClkInitStruct = { 0 };

	/** Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
	RCC_OscInitStruct.HSEState = RCC_HSE_ON;
	RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
	RCC_OscInitStruct.HSIState = RCC_HSI_ON;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
	RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
		Error_Handler();
	}
	/** Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK
			| RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK) {
		Error_Handler();
	}
}

void Error_Handler(void) {

}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
