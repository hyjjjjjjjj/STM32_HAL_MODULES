/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 * 					实现通过定时器1的通道1输出频率为72M/72/100的PWM波
 * 					将战舰板的PA8与PB5短接，可实现LED呼吸灯
 ******************************************************************************
 *  Created on: Jul 2, 2020
 *      Author: HYJ
 *******************************************************************************/
#include "main.h"
#include "tim.h"
#include "gpio.h"
#include "usart.h"

void SystemClock_Config(void);

struct TIMER_COMMON *tim1, *tim2;
struct IC_CAP_COMMON *cap_in;
extern struct UART_COMMON *uart1_common;

int main(void) {
	u32 pwm_duty = 0;
	long long temp = 0;
	HAL_Init();
	SystemClock_Config();

	uart1_common = new_Uart(uart1_common, USART1, 115200);	//初始化串口一 注册相关成员函数
	uart1_common->UPrintf(uart1_common, "BASIC TIMER EXAMPLE!\r\n");

	tim1 = new_TIMER(tim1, TIM1, IC_CAPTURE_MODE, 71, 0xffff);			//定时器初始化
	tim1->AddICAPCh(tim1,TIM_CHANNEL_1, GPIOA, GPIO_PIN_8);
	cap_in = &tim1->ic_cap[TIM_CHANNEL_1];
	cap_in->StartIT(cap_in);
	__HAL_TIM_ENABLE_IT(&tim1->tim_handle,TIM_IT_UPDATE);   //使能更新中断

	tim2 = new_TIMER(tim2, TIM2, PWM_MODE, 71, 99);					//定时器初始化
	tim2->AddPWMCh(tim2, TIM_CHANNEL_4, GPIOA, GPIO_PIN_3);
	PWM_COMMON *pwm_output = &tim2->pwm_out[TIM_CHANNEL_4 / 4];		//PWM输出类
	pwm_output->Start(pwm_output);
	pwm_output->SetDuty(pwm_output, 90);						//输出频率为100Hz的方波

	while (1) {
		if (pwm_duty >= 90)
			pwm_duty = 0;
		else
			pwm_duty += 1;
//		pwm_output->SetDuty(pwm_output, pwm_duty);
		if (cap_in->cap_sta & 0X80)        //成功捕获到了一次高电平
				{
			temp = cap_in->cap_sta & 0X3F;
			temp *= 0XFFFFFFFF;		 	    //溢出时间总和
			temp += cap_in->cap_value;      //得到总的高电平时间
			uart1_common->UPrintf(uart1_common,"HIGH:%lld us\r\n", temp);      //打印总的高点平时间
			cap_in->cap_sta = 0;          //开启下一次捕获
		}
		HAL_Delay(10);
	}
}

/**
 * @brief System Clock Configuration
 * @retval None
 */
void SystemClock_Config(void) {
	RCC_OscInitTypeDef RCC_OscInitStruct = { 0 };
	RCC_ClkInitTypeDef RCC_ClkInitStruct = { 0 };

	/** Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
	RCC_OscInitStruct.HSEState = RCC_HSE_ON;
	RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
	RCC_OscInitStruct.HSIState = RCC_HSI_ON;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
	RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
		Error_Handler();
	}
	/** Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK
			| RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK) {
		Error_Handler();
	}
}

/* USER CODE BEGIN 4 */

void TIM1_UP_IRQHandler(void) {
	HAL_TIM_IRQHandler(&tim1->tim_handle);
}

void TIM1_CC_IRQHandler(void) {
	HAL_TIM_IRQHandler(&tim1->tim_handle);
}

//定时器周期溢出回调函数，该函数在HAL_TIM_IRQHandler中会被调用
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim) {
	if (htim->Instance == TIM1) {
		if ((cap_in->cap_sta & 0X80) == 0)				//还未成功捕获
				{
			if (cap_in->cap_sta & 0X40)				//已经捕获到高电平了
					{
				if ((cap_in->cap_sta & 0X3F) == 0X3F)				//高电平太长了
						{
					cap_in->cap_sta |= 0X80;		//标记成功捕获了一次
					cap_in->cap_value = 0XFFFFFFFF;
				} else
					cap_in->cap_sta++;
			}
		}
	}
}

//定时器输入捕获中断处理回调函数，该函数在HAL_TIM_IRQHandler中会被调用
void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef *htim)			//捕获中断发生时执行
{
	if (htim->Instance == TIM1) {
		if ((cap_in->cap_sta & 0X80) == 0)				//还未成功捕获
				{
			if (cap_in->cap_sta & 0X40)		//捕获到一个下降沿
					{
				cap_in->cap_sta |= 0X80;		//标记成功捕获到一次高电平脉宽
				cap_in->cap_value = cap_in->ReadCapVal(cap_in);		//获取当前的捕获值.
				cap_in->Reset(cap_in);							//一定要先清除原来的设置！！
				cap_in->SetConfig(cap_in, TIM_ICPOLARITY_RISING);//配置TIM5通道1上升沿捕获
			} else  								//还未开始,第一次捕获上升沿
			{
				cap_in->cap_sta = 0;			//清空
				cap_in->cap_value = 0;
				cap_in->cap_sta |= 0X40;		//标记捕获到了上升沿
				__HAL_TIM_DISABLE(&tim1->tim_handle);        //关闭定时器5
				tim1->SetCNT(tim1, 0);
				cap_in->Reset(cap_in);							//一定要先清除原来的设置！！
				cap_in->SetConfig(cap_in, TIM_ICPOLARITY_FALLING);//配置TIM5通道1上升沿捕获
//				__HAL_TIM_SET_COUNTER(&TIM5_Handler, 0);
//				TIM_RESET_CAPTUREPOLARITY(&TIM5_Handler, TIM_CHANNEL_1); //一定要先清除原来的设置！！
//				TIM_SET_CAPTUREPOLARITY(&TIM5_Handler, TIM_CHANNEL_1,
//						TIM_ICPOLARITY_FALLING);        //定时器5通道1设置为下降沿捕获
				__HAL_TIM_ENABLE(&tim1->tim_handle);        //使能定时器5
			}
		}
	}

}

/* USER CODE END 4 */

/**
 * @brief  This function is executed in case of error occurrence.
 * @retval None
 */
void Error_Handler(void) {
	/* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */

	/* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
