/*
 * tim.c
 *
 *  Created on: Jul 2, 2020
 *      Author: HYJ
 */
#ifndef __tim_H
#define __tim_H
#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "gpio.h"

extern TIM_HandleTypeDef htim1;

typedef struct PWM_COMMON {
	TIM_HandleTypeDef tim_handle;
	u32 channel;
	void (*SetDuty)(struct PWM_COMMON *this, u32 compare);
	void (*Start)(struct PWM_COMMON *this);
	void (*Stop)(struct PWM_COMMON *this);
} PWM_COMMON;

typedef struct IC_CAP_COMMON {
	TIM_HandleTypeDef tim_handle;
	u32 channel;
	u8 cap_sta;
	u32 cap_value;
	void (*StartIT)(struct IC_CAP_COMMON *this);
	void (*StopIT)(struct IC_CAP_COMMON *this);
	void (*Start)(struct IC_CAP_COMMON *this);
	void (*Stop)(struct IC_CAP_COMMON *this);
	void (*Reset)(struct IC_CAP_COMMON *this);
	u32 (*ReadCapVal)(struct IC_CAP_COMMON *this);
	void (*SetConfig)(struct IC_CAP_COMMON *this, u32 mode);
} IC_CAP_COMMON;

typedef struct TIMER_COMMON {
	TIM_HandleTypeDef tim_handle;
	PWM_COMMON pwm_out[4];
	IC_CAP_COMMON ic_cap[4];
	u32 (*GetCNT)(struct TIMER_COMMON *this);
	void (*SetCNT)(struct TIMER_COMMON *this, u32 cnt);
	void (*SetPSC)(struct TIMER_COMMON *this, u32 psc);
	void (*SetARR)(struct TIMER_COMMON *this, u32 arr);
	void (*StartIT)(struct TIMER_COMMON *this);
	void (*StopIT)(struct TIMER_COMMON *this);
	void (*Start)(struct TIMER_COMMON *this);
	void (*Stop)(struct TIMER_COMMON *this);

	void (*AddPWMCh)(struct TIMER_COMMON *this, uint32_t Channel,
			GPIO_TypeDef *port, uint32_t pin);
	void (*AddICAPCh)(struct TIMER_COMMON *this, uint32_t Channel,
			GPIO_TypeDef *port, uint32_t pin);


} TIMER_COMMON;

enum TIMER_MODE {
	BASE_MODE = 1, PWM_MODE, IC_CAPTURE_MODE
};

TIMER_COMMON* new_TIMER(struct TIMER_COMMON *this, TIM_TypeDef *tim, u8 mode,
		u32 psc, u32 arr);

#ifdef __cplusplus
}
#endif
#endif /*__ tim_H */

