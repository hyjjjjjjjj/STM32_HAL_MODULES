/*
 * dht11.h
 *
 *  Created on: Jul 16, 2020
 *      Author: zddd
 */

#ifndef __DHT11_H_
#define __DHT11_H_


#include "main.h"


GPIO_COMMON *dht11_in;
GPIO_COMMON *dht11_out;


#define DHT11_PORT		GPIOA
#define DHT11_PIN		GPIO_PIN_11

#define DHT11_OUT_1				dht11_out->WritePin(dht11_out,GPIO_PIN_SET)
#define DHT11_OUT_0				dht11_out->WritePin(dht11_out,GPIO_PIN_RESET)
//
#define DHT11_IN				dht11_in->ReadPin(dht11_in)

typedef struct
{
	float humi;				// 湿度
	float temp;	 			// 温度
	uint8_t error_code;           //初始化是否成功
	uint8_t check_sum;	 		// 校验和
    GPIO_COMMON DHT11_Pin;                                               //DHT11用到的GPIO
    void (*DHT11_Read)(struct DHT11_Data_TypeDef *this);       //读取DHT11数据的函数入口指针
} DHT11_Data_TypeDef;

uint8_t DHT11_ReadData(DHT11_Data_TypeDef* DHT11_Data);
void delay_us(uint32_t nus);


#endif /* INC_DHT11_H_ */
