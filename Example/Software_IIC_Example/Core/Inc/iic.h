/*
 * iic.h
 *
 *  Created on: Sep 27, 2020
 *      Author: 16330
 */

#ifndef INC_IIC_H_
#define INC_IIC_H_

#include "main.h"
#include "gpio.h"

#define IIC_NO_ACK  1
#define IIC_ACK     0

typedef struct IIC_COMMON{
	GPIO_COMMON *scl;
	GPIO_COMMON *sda;
	u8 addr;
	void (*I2C_Stop)(struct IIC_COMMON *this);
	void (*I2C_Start)(struct IIC_COMMON *this);
	void (*I2C_Send_Byte)(struct IIC_COMMON *this,u8 data);
	uint8_t (*I2C_Recieve_Byte)(struct IIC_COMMON *this);
	void (*WriteOneByte)(struct IIC_COMMON *this,uint8_t WriteAddr, uint8_t DataToWrite);
	uint8_t (*ReadOneByte)(struct IIC_COMMON *this,uint8_t ReadAddr);
	void (*ReadBytes)(struct IIC_COMMON *this,uint8_t ReadAddr,uint8_t *Buffer,uint16_t Num);
	void (*WriteBytes)(struct IIC_COMMON *this,uint8_t WriteAddr,uint8_t *Buffer,uint16_t Num);
}IIC_COMMON;

IIC_COMMON* new_IIC(struct IIC_COMMON *this ,GPIO_TypeDef  *sda_port,uint32_t sda_pin,GPIO_TypeDef  *scl_port,uint32_t scl_pin,uint8_t s_addr);		//串口初始化，port参数填入u8类型的UART_PORT枚举变量

#endif /* INC_IIC_H_ */
